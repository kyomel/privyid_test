'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bank_balance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Bank_balance.hasMany(models.Bank_balance_history, {
        foreignKey:'bankBalanceId'
      })
    }
  };
  Bank_balance.init({
    balance: DataTypes.INTEGER,
    balance_achieve: DataTypes.INTEGER,
    code: DataTypes.STRING,
    enable: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Bank_balance',
  });
  return Bank_balance;
};