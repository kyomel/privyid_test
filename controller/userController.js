const index = require('../models');
const user = index.User;
const response = require('../helper/response');
const bcrypt = require('bcrypt');
const token = require('../helper/token');
const { Op } = require("sequelize");

module.exports = {
    async login(req, res) {
        try {
            let username = req.body.username?req.body.username:req.body.email.toLowerCase()
            let instanceLogin = await user.findOne({
               where: {
                [Op.or]: [{ email: username }, { username: username }]
               }
            })
            
            if(!instanceLogin) {
                throw new Error(`Username or Email doesn't exist!`)
            }
            const isPasswordTrue = await bcrypt.compareSync(req.body.password, instanceLogin.password)
            if(!isPasswordTrue) {
                throw new Error(`Wrong Password!`)
            }
            return res.status(201).json(response.success('Successfully login!', { token: token(instanceLogin), User: instanceLogin}))
        } catch(err) {
            return res.status(403).json(response.error('Login Fail!', err.message))
        }
    },

    async userLogin(req, res){
        let result = await user.findOne({
            where: {
                id: req.user.id
            },
            attributes: ['id','username','email']
        })
        return res.status(201).json(response.success('This account is active!', {Result: result}))
    }
}