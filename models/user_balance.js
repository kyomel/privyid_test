'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_balance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_balance.belongsTo(models.User, {
        foreignKey:'userId'
      }),
      User_balance.hasMany(models.User_balance_history, {
        foreignKey:'userBalanceId'
      })
    }
  };
  User_balance.init({
    userId: DataTypes.INTEGER,
    balance: DataTypes.INTEGER,
    balanceAchieve: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_balance',
  });

  User_balance.topup = async (data) => {
    const { userId, money } = data
    if (!Number.isInteger(money)) return Promise.reject(new Error('Money must be number!'))
    const pastBalance = await User_balance.findOne({
      where: {
        userId
      }
    })
    const pay = await User_balance.increment('balance', {
      by: money,
      where: {
        userId
      }
    })
    const balance = await User_balance.findOne({
      where: {
        userId
      }
    })

    await User_balance.historyTopup({
      userBalanceId: balance.isSoftDeleted,
      balance_before: pastBalance.balance,
      balance_after: balance.balance
    })

    return balance
  }

  User_balance.transfer = async (data) => {
    const { username, money, currentUserId } = data
    const userTarget = await sequelize.models.User.findOne({
      where: {
        username
      }
    })
    if(!userTarget) return Promise.reject(new Error('User is not exist'))
    if(!Number.isInteger(money)) return Promise.reject(new Error('Money must be number'))

    const balanceTarget = await User_balance.findOne({
      where: {
        userId: userTarget.id
      }
    })
    const currentUser = await User_balance.findOne({
      where: {
        userId: currentUserId
      }
    })
    if(money > currentUserId.balance) return Promise.reject(new Error('Your balance is not enough'))
    await User_balance.increment('balance', {
      by: money,
      where: {
        userId: userTarget.id
      }
    })

    await User_balance.decrement('balance', {
      by: money,
      where: { userId: currentUserId}
    })

    data = {
      targetUserId: userTarget.id,
      pastTargetBalance: balanceTarget.balance,
      currentUserId,
      pastUserBalance: currentUser.balance
    }

    const result = await User_balance.historyTransfer(data)
    return result
  }

  User_balance.historyTopup = async (data) => {
    const { userBalanceId, balanceBefore, balanceAfter } = data
    const topup = await sequelize.models.User_balance_history.create({
      userBalanceId,
      balance_before,
      balance_after,
      activity: 'Topup Balance',
      type: 'debit',
      ip: '1780911',
      location: 'Jogja',
      userAgent:'Agus',
      author:'Kyomel'
    })

    return topup
  }

  User_balance.historyTransfer = async (data) => {
    const { targetUserId, pastTargetBalance, currentUserId, pastUserBalance } = data
    const target = await User_balance.findOne({
       where: { 
         userId: targetUserId 
        },
        include: ['User'] 
    })
    const current = await User_balance.findOne({ 
      where: 
      { 
        userId: currentUserId 
      }, 
      include: ['User'] 
    })
    const history = await sequelize.models.User_balance_history.bulkCreate([
      {
        user_balance_id: target.id,
        balance_before: pastTargetBalance,
        balance_after: target.balance,
        activity: `Transfer From ${current.User.username}`,
        type: 'debit',
        ip: '1780912',
        location: 'Solo',
        userAgent:'Purnomo',
        author:'Galang'
      },
      {
        user_balance_id: current.id,
        balance_before: pastUserBalance,
        balance_after: current.balance,
        activity: `Transfer To ${target.User.username}`,
        type: 'credit',
        ip: '1780911',
        location: 'Jogja',
        userAgent:'Agus',
        author:'Kyomel'
      }
    ], { returning: true })
    return history[1]
  }
  return User_balance;
};