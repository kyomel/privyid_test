'use strict';
const bcrypt = require('bcrypt');
const User = require('../models/user');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('Users',[{
      username: 'test',
      email: 'test@mail.com',
      password: bcrypt.hashSync('12345678', bcrypt.genSaltSync(10)),
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      username: 'test01',
      email: 'test01@mail.com',
      password: bcrypt.hashSync('12345678', bcrypt.genSaltSync(10)),
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
