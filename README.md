# Pretest Privyid

This is server-side application based on Node.js to finish Pretest at Privyid to become they Backend developer team.

## What do I use here:
1. Node.js to run javascript in server-side
2. Framework and Library:
    - ```bcryptjs``` Used for hash password become encrpyted password and validate password with encrypted password in database
    - ```dotenv``` Used to loads environment variables from .env file into process.env 
    - ```express``` framework to build RESTful API
    - ```jsonwebtoken``` Used to generate token that will be use for authentication
    - ```morgan``` Used for logging HTTP request
    - ```pg``` Non-blocking PostgreSQL client for Node.js
    - ```pg-hstore``` Used for serializing and deserializing JSON data to hstore forma
    - ```sequelize``` Used for ORM in Node.js
    - ```sequelize-cli``` Used for command line interface to execute sequlieze

## How to run this:
1. Make sure you already install Node.js in your device
2. Install postgreSQL, sequelize and sequelize-cli in your device
3. Clone this repository using Git
4. Move to directory you have been clone
5. Create file ```.env``` and filled in like on the ```env.example```
6. Run ```npm install``` to install all dependecies
7. Run ```sequelize db:create``` to create database
8. Run ```sequelize db:migrate``` to execute migration file and generate table into our database
9. Run ```sequelize db:seed:all``` to execute seeder file and generate demo user
10. Last but not least you can run this server-side using ```nmp start``` or ```npm run dev```