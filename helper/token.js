const jwt = require('jsonwebtoken');

module.exports = (user) =>
    jwt.sign({
        id: user.id,
        username: user.username,
        email: user.email

    }, process.env.SECRET_KEY);