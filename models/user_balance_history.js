'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_balance_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_balance_history.belongsTo(models.User_balance, {
        foreignKey:'userBalanceId'
      })
    }
  };
  User_balance_history.init({
    userBalanceId: DataTypes.INTEGER,
    balanceBefore: DataTypes.INTEGER,
    balanceAfter: DataTypes.INTEGER,
    activity: DataTypes.STRING,
    type: DataTypes.ENUM('debit','credit'), //nanti ganti enum sabar yak
    ip: DataTypes.STRING,
    location: DataTypes.STRING,
    userAgent: DataTypes.STRING,
    author: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_balance_history',
  });
  return User_balance_history;
};