const router = require('express').Router();
const userController = require('./controller/userController');
const balanceContoller = require('./controller/balaceController');
const { authenticate } = require('./middleware/authenticate');
const balaceController = require('./controller/balaceController');

//User API
router.post('/user/login', userController.login);
router.get('/user/me', authenticate, userController.userLogin);

//Balance API
router.post('/', authenticate, balaceController.topup);
router.post('/transfer', authenticate, balanceContoller.transfer);
router.get('/', authenticate, balanceContoller.showBalance);
router.get('/history', authenticate , balaceController.showHistory);

module.exports = router;