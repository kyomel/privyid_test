const index = require('../models');
const user_balance = index.User_balance;
const user_balance_history = index.User_balance_history;
const response = require('../helper/response');
const { use } = require('../router');

module.exports = {
    async topup(req, res) {
        try {
            const data = {
                userId: req.user.id,
                money: req.body.money
            }
            const result = await user_balance.topup(data)
            return res.status(200).json(response.success('Successfully top up!', { data: result}))
        } catch(err) {
            return res.status(422).json(response.error('Top up fail!', err.message))
        }
    },

    async transfer(req, res) {
        try {
            const data = {
                username: req.body.username,
                money: req.body.money,
                currentUserId: req.user.id
            }
            const result = await user_balance.transfer(data)
            return res.status(200).json(response.success('Successfully transfer!', { data: result}))
        } catch(err) {
            return res.status(422).json(response.error('Transfer fail!', err.message))
        }
    },

    async showBalance(req, res) {
        try {
            const balance = await user_balance.findOne({
                where: {
                    userId: req.user.id
                }
            })
            return res.status(200).json(response.success('Successfully check your balance', { data: balance}))
        } catch(err) {
            return res.status(422).json(response.error('Check balance fail!', err.message))
        }
    },

    async showHistory(req, res) {
        try {
            const history = await user_balance_history.findAll({
                order: [['createdAt', 'DESC']],
                include: [{
                    model: User_balance,
                    where: {
                        userId: req.user.id
                    },
                    attributes: []
                }]
            })
            return res.status(200).json(response.success('Successfully check your history', { data: history}))
        } catch(err) {
            return res.status(500).json(response.error('Check history fail!', err.message))
        }
    }
}